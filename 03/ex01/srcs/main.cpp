#include "../includes/FragTrap.hpp"
#include "../includes/ScavTrap.hpp"


int     main(void)
{
    FragTrap bot("Kriz_eko");
    std::string target = "Jeune entrepreneur";
    bot.rangedAttack(target);
    bot.takeDamage(50);
    bot.beRepaired(10);
    bot.takeDamage(20);
    bot.meleeAttack(target);

    bot.vaulthunter_dot_exe(target);
    bot.vaulthunter_dot_exe(target);
    bot.vaulthunter_dot_exe(target);
    bot.vaulthunter_dot_exe(target);
    bot.vaulthunter_dot_exe(target);

    bot.beRepaired(8000);
    bot.takeDamage(300);

    std::cout << "----------------------------------\n";

    ScavTrap bot2("Kriz_zanit_hair");
    std::string target2 = "Personne agée";
    bot2.rangedAttack(target2);
    bot2.takeDamage(50);
    bot2.beRepaired(10);
    bot2.takeDamage(20);
    bot2.meleeAttack(target2);

    bot2.challengeNewcomer(target2);
    bot2.challengeNewcomer(target2);
    bot2.challengeNewcomer(target2);
    bot2.challengeNewcomer(target2);
    bot2.challengeNewcomer(target2);

    bot2.beRepaired(8000);
    bot2.takeDamage(300);
    return (0);
}