#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

#include "FragTrap.hpp"

class ScavTrap {

    public:
        ScavTrap(std::string name);
        ~ScavTrap(void);

        // méthodes
        void rangedAttack(std::string const & target);
        void meleeAttack(std::string const & target);
        void takeDamage(unsigned int amount);
        void beRepaired(unsigned int amount);

        void challengeNewcomer(std::string const& target);


    private:
        // attributs
        unsigned int _hit_point;
        unsigned int _max_hit_point;
        unsigned int _enegery_point;
        unsigned int _max_energy_point;
        unsigned int _level;
        unsigned int _melee_attack_dmg;
        unsigned int _ranged_attack_dmg;
        unsigned int _armor_dmg_reduction;
        std::string _name;
};

#endif