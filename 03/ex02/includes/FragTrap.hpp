#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

#include "ClapTrap.hpp"

#include <iostream>
#include <time.h>

class FragTrap : public ClapTrap {

    public:
        // constructeur / destructeur
        FragTrap(std::string name);
        ~FragTrap(void);
 
        //méthodes
        void vaulthunter_dot_exe(std::string const & target);
    
    private:
        FragTrap();
};

#endif