#include "../includes/FragTrap.hpp"


//---------------------------------------------------
// 			CONSTRUCTEURS/DECONSTRUCTEURS 			|
//---------------------------------------------------

// constructeur - initialisation des variables
FragTrap::FragTrap(std::string name) : ClapTrap(name)
{
    this->_hit_point = 100;
    this->_max_hit_point = 100;
    this->_enegery_point = 100;
    this->_max_energy_point = 100;
    this->_level = 1;
    this-> _melee_attack_dmg = 30;
    this->_ranged_attack_dmg = 20;
    this->_armor_dmg_reduction = 5;
    this->_type = "*FR4G-TP";
    std::cout << "\033[36mFragTrap - Constructor is called\033[m" << std::endl;
    srand(time(0));
}

// destructeur
FragTrap::~FragTrap(void){
    std::cout << "\033[36mFragTrap - Destructor is called\033[m" << std::endl;
}

//---------------------------------------------------
// 						METHODES					|
//---------------------------------------------------

static std::string r_attack[5] = { "Attaque trempette", 
                            "Prendre la fuite",
                            "Attaque éclair",
                            "Attaque contrôle mental",
                            "Attaque flamiche" };
void
FragTrap::vaulthunter_dot_exe(std::string const& target){
    unsigned int& hit_p = this->_hit_point;
    unsigned int max_hit_p = this->_max_hit_point;
    unsigned int cost = 25;
    unsigned int random = rand() % 5;

    if (hit_p < cost)
        std::cout << "\033[36mPas assez d'énergie pour lancer une autre attaque random\n\033[m";
    else
        {
            std::cout << "\033[35m" << this->_type << " " << this->_name << " lance " 
            << r_attack[random] << " sur " << target
            << " et dépense " << cost << " d'énergie."
            << "(énergie = " << (hit_p -= cost) << "/" << max_hit_p << ")" << std::endl;
        }
    return ;
}
