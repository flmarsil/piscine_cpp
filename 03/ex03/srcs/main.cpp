#include <cstdlib>
#include "../includes/FragTrap.hpp"
#include "../includes/ScavTrap.hpp"
#include "../includes/NinjaTrap.hpp"

int main()
{
	srand(time(0));

	std::cout << "---------- FRAGTRAP TESTS ----------\n\n";
	
	FragTrap idiot("Idiot");
	idiot.meleeAttack("Handsome Jack");
	idiot.rangedAttack("Handsome Jack");
	std::cout << std::endl;

	idiot.takeDamage(1);
	idiot.takeDamage(40);
	idiot.beRepaired(30);
	idiot.beRepaired(100);
	
	std::cout << std::endl;
	idiot.vaulthunter_dot_exe("Handsome Jack");
	idiot.vaulthunter_dot_exe("Baron Flynt");
	idiot.vaulthunter_dot_exe("Roid Rage Psycho");
	idiot.vaulthunter_dot_exe("Pinky");
	idiot.vaulthunter_dot_exe("Mad Mel");
	
	std::cout << std::endl;
	idiot.beRepaired(50);
	idiot.beRepaired(100);


	std::cout << "\n\n---------- SCAVTRAP TESTS ----------\n\n";
	
	ScavTrap dumbass("Dumbass");
	dumbass.meleeAttack("Handsome Jack");
	dumbass.rangedAttack("Handsome Jack");
	std::cout << std::endl;

	dumbass.takeDamage(1);
	dumbass.takeDamage(40);
	dumbass.beRepaired(30);
	dumbass.beRepaired(100);
	
	std::cout << std::endl;
	dumbass.challengeNewcomer("Handsome Jack");
	dumbass.challengeNewcomer("Baron Flynt");
	dumbass.challengeNewcomer("Roid Rage Psycho");
	dumbass.challengeNewcomer("Pinky");
	dumbass.challengeNewcomer("Mad Mel");
	
	std::cout << std::endl;
	dumbass.beRepaired(50);
	dumbass.beRepaired(100);


	std::cout << "\n\n---------- CLAPTRAP TESTS ----------\n\n";
	
	ClapTrap stupid("Stupid");
	stupid.meleeAttack("Handsome Jack");
	stupid.rangedAttack("Handsome Jack");
	std::cout << std::endl;

	stupid.takeDamage(1);
	stupid.takeDamage(40);
	stupid.beRepaired(30);
	stupid.beRepaired(100);
	
	std::cout << std::endl;
	stupid.beRepaired(50);
	stupid.beRepaired(100);
	std::cout << std::endl;

	std::cout << "\n\n---------- NINJATRAP TESTS ----------\n\n";

	NinjaTrap retard("Retard");
	retard.meleeAttack("Handsome Jack");
	retard.rangedAttack("Handsome Jack");
	std::cout << std::endl;

	retard.takeDamage(1);
	retard.takeDamage(40);
	retard.beRepaired(30);
	retard.beRepaired(100);
	
	std::cout << std::endl;
	retard.ninjaShoebox(idiot);
	retard.ninjaShoebox(dumbass);
	retard.ninjaShoebox(stupid);
	retard.ninjaShoebox(retard);
	
	std::cout << std::endl;
	retard.beRepaired(50);
	retard.beRepaired(100);
	std::cout << std::endl;

	return (0);
}