#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

#include "ClapTrap.hpp"

#include <iostream>
#include <time.h>

class ScavTrap : public ClapTrap {

    public:
        // constructeur / destructeur
        ScavTrap(std::string name);
        ScavTrap();
        ~ScavTrap(void);

        //méthodes
        void challengeNewcomer(std::string const& target);
};

#endif