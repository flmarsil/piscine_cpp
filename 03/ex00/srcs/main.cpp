#include "../includes/FragTrap.hpp"

int     main(void)
{
    srand(time(0));

    FragTrap bot("Kriz_eko");
    std::string target = "Jeune entrepreneur";
    bot.rangedAttack(target);
    bot.takeDamage(50);
    bot.beRepaired(10);
    bot.takeDamage(20);
    bot.meleeAttack(target);

    bot.vaulthunter_dot_exe(target);
    bot.vaulthunter_dot_exe(target);
    bot.vaulthunter_dot_exe(target);
    bot.vaulthunter_dot_exe(target);
    bot.vaulthunter_dot_exe(target);

    bot.beRepaired(8000);
    bot.takeDamage(300);
    return (0);
}