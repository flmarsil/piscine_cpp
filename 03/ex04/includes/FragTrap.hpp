#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

#include "ClapTrap.hpp"

#include <iostream>
#include <time.h>

class FragTrap : public virtual ClapTrap {

    public:
        // constructeur / destructeur
        FragTrap(std::string name);
        ~FragTrap();

        //méthodes
        void vaulthunter_dot_exe(std::string const & target);
        void getVar(unsigned int var) const;
        void printattr() const;

    private:
        FragTrap();

};

#endif