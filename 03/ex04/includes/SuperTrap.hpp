#ifndef SUPERTRAP_HPP
#define SUPERTRAP_HPP

#include "FragTrap.hpp"
#include "NinjaTrap.hpp"
#include <iostream>

class SuperTrap : public NinjaTrap, public FragTrap {

    public:
        // constructeur & destructeur
        SuperTrap(std::string name);
        ~SuperTrap();

        void rangedAttack(const std::string & target);
	    void meleeAttack(const std::string & target);
        void printattr() const;    
};

#endif
