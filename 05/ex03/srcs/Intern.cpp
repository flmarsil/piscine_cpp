#include "../includes/Intern.hpp"

/* Constructor */
Intern::Intern() {}

/* Destructor */
Intern::~Intern() {}

/* Copy constructor */
Intern::Intern(const Intern& copyObj)
{
    (void)copyObj;
}

/* Overloading operator */
Intern& Intern::operator = (const Intern& assignObj) 
{
    (void)assignObj;
    return (*this);
}

/* Methods */

static int lenList = 3;

static const std::string List[3] =
{
    "ShrubberyCreationForm",
    "RobotomyRequestForm",
    "PresidentialPardonForm"
};

Form* Intern::MakeForm(const std::string& nameForm,  const std::string& targetForm) const
{
    Form* Ret(NULL);
    
    // function pointer array
    Form* (Intern::*function_ptr[3])(const std::string& targetForm) const =
    {
        &Intern::make_shrub,
        &Intern::make_rob,
        &Intern::make_pres
    };

    for (int i = 0 ; i < lenList ; i++)
        if (nameForm == List[i])
            Ret = (this->*function_ptr[i])(targetForm);
    if (Ret == NULL)
        throw NotFoundException();
    return (Ret);
}

Form* Intern::make_shrub(const std::string &target) const
{
    std::cout 
        << "Intern creates ShrubberyCreationForm"
        << std::endl;
    return (new ShrubberyCreationForm(target));
}

Form* Intern::make_rob(const std::string &target) const
{
    std::cout 
        << "Intern creates RobotomyRequestForm"
        << std::endl;
    return (new RobotomyRequestForm(target));
}

Form* Intern::make_pres(const std::string &target) const
{
    std::cout 
        << "Intern creates PresidentialPardonForm"
        << std::endl;
    return (new PresidentialPardonForm(target));
}

/* Exception */

Intern::NotFoundException::NotFoundException() throw() {}

const char* Intern::NotFoundException::what() const throw()
{
    return ("Intern::NotFoundException found\n");
}