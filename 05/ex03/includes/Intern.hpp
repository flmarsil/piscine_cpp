#ifndef INTERN_HPP
#define INTERN_HPP

#include "Form.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"

#include <iostream>
#include <exception>
#include <cstring>

class Intern
{
    public:
        /* Constructor */
            Intern();
        /* Destructor */
            ~Intern();
        /* Copy constructor */
            Intern(const Intern& copyObj);
        /* Overloading operator */
            Intern& operator = (const Intern& assignObj);
        /* Methods */
            Form* MakeForm(const std::string& nameForm, const std::string& targetForm) const;
        /* Classe exception */
            class NotFoundException : public std::exception
            {
                public :
                    NotFoundException() throw();
                    virtual const char* what() const throw();
            };

    private:
        
        /* Private methods */
            Form* make_shrub(const std::string &target) const;
            Form* make_rob(const std::string &target) const;
            Form* make_pres(const std::string &target) const;


};

#endif
