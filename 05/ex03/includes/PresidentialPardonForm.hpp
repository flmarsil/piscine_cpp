#ifndef PRESIDENTIALPARDONFORM_HPP
#define PRESIDENTIALPARDONFORM_HPP

#include "Form.hpp"

#include <iostream>

class PresidentialPardonForm : public Form
{
    public:
        /* Constructor */
            PresidentialPardonForm(const std::string& target);
        /* Destructor */
            virtual ~PresidentialPardonForm();
        /* Copy constructor */
            PresidentialPardonForm(const PresidentialPardonForm& copyObj);
        /* Overloading operator */
            PresidentialPardonForm& operator = (const PresidentialPardonForm& assignObj);
        /* Methods */
            virtual void execute (Bureaucrat const & executor) const;
        
    private:
        /* Private constructor */
            PresidentialPardonForm();
};

#endif
