#include "../includes/Bureaucrat.hpp"

int main()
{
    // Test with too low downgrade
    Bureaucrat B1("Charles", 150);
    try 
    {
        std::cout << B1 << std::endl;
        B1.downGrade();
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    // Test with too high upgrade
    Bureaucrat B2("Pierre", 1);
    try 
    {
        std::cout << B2 << std::endl;
        B2.upGrade();
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    // Test with too high creation grade
    try
    {
        Bureaucrat B3("Jésus", 0);
        std::cout << B3 << std::endl;
    }

    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    // Test with too low creation grade
    try
    {
        Bureaucrat B4("Judas", 165);
        std::cout << B4 << std::endl;
    }

    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
    
    return (0);
}