#ifndef FORM_HPP
#define FORM_HPP

#include "Bureaucrat.hpp"

#include <iostream>
#include <exception>

class Bureaucrat;

class Form 
{
    public:
        /* Constructor */
            Form(const std::string& name, int gradeForSign, int gradeForExec);
        /* Destructor */
            ~Form();
        /* Copy constructor */
            Form(const Form& copyObj);
        /* Overloading operator */
            Form& operator = (const Form& assignObj);
        /* Methods */
            int getGradeForSign() const;
            int getGradeForExec() const;
            std::string getName() const;
            bool getSigned() const;
            void beSigned(const Bureaucrat& bureaucrat);

        /* Classes exceptions */
            class GradeTooHighException : public std::exception
            {
                public :
                    GradeTooHighException() throw();
                    virtual const char* what() const throw();
            };

            class GradeTooLowException : public std::exception
            {
                public :
                    GradeTooLowException() throw();
                    virtual const char* what() const throw();
            };

            class AlreadySigned: public std::exception
            {
                public :
                    AlreadySigned() throw();
                    virtual const char *what() const throw();
            };

    private:
            Form();
        /* Attributs */
            const std::string   _name;
            bool                _is_signed;
            const int           _gradeForSign;  // 1 =< grade <= 150
            const int           _gradeForExec;  // 1 =< grade <= 150
};

std::ostream& operator << (std::ostream& flux, const Form& form);

#endif
