#include "../includes/Bureaucrat.hpp"
#include "../includes/Form.hpp"

int main()
{
	std::cout << "----------------------------------------------\n";
	std::cout << "-------------- BUREAUCRAT TESTS --------------\n";
	std::cout << "----------------------------------------------\n";
	
	Bureaucrat henri("Henri", 1);

	std::cout << henri;
	
	// GradeTooHigh exception test
	try
	{
		std::cout << "Let's try to increase " << henri.getName() << "'s grade!\n";
		henri.upGrade();
		std::cout << "Result: " << henri;
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}
	
	// GradeTooLow exception test
	try
	{
		std::cout << "Let's try to lower " << henri.getName() << " at 200\n";
		for (int i = 0; henri.getGrade() < 200; i++)
			henri.downGrade();
		std::cout << "Result: " << henri;
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}
	
	// Constructor GradeTooLow exception test
	try
	{
		std::cout << "Let's create a bureaucrat with a grade of 180\n";
		Bureaucrat jim("Jim", 180);
		std::cout << jim;
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}
	

	std::cout << "----------------------------------------------\n";
	std::cout << "----------------- FORM TESTS -----------------\n";
	std::cout << "----------------------------------------------\n";

	// Succesfull creation
	try
	{
		std::cout << "Creating a form with good parameters\n";
		Form f3("F3", 30, 20);
		std::cout << "Form succesfully created\n";
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}
	
	// Creation failed : exception GradeTooLow
	try
	{
		std::cout << "Creating a form with wrong parameters\n";
		Form f3("F3", 30, 180);
		std::cout << "Form succesfully created\n";
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}


	Form f1("F1", 55, 22);
	Form f2(f1);
	Form f3("F3", 30, 20);
	
	// Testing getters
	std::cout << "Name:" << f3.getName() << "\n";
	std::cout << "Is signed value:" << f3.getSigned() << "\n";
	std::cout << "Grade to sign:" << f3.getGradeForSign() << "\n";
	std::cout << "Grade to execute:" << f3.getGradeForExec() << "\n\n";

	// Testing overload operator<<
	std::cout << f1 << f2 << f3 << "\n";
	

    // beSigned failed
	Bureaucrat Lola("Lola", 144);
	try
	{
        f3.beSigned(Lola);
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}

	// beSigned works
	Bureaucrat jimmy("Jimmy", 1);
	try
	{
		f1.beSigned(jimmy);
        f2.beSigned(jimmy);
        f3.beSigned(jimmy);
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}
	
    std::cout << f1 << f2 << f3 << "\n";
	return (0);
}