#include "../includes/ShrubberyCreationForm.hpp"

/* Constructor */
ShrubberyCreationForm::ShrubberyCreationForm(const std::string& target)
    : Form("Shrubbery", 145, 137, target) {}

/* Destructor */
ShrubberyCreationForm::~ShrubberyCreationForm() {}

/* Copy constructor */
ShrubberyCreationForm::ShrubberyCreationForm(const ShrubberyCreationForm& copyObj)
    :   Form(copyObj) {}

/* Overloading operator */
ShrubberyCreationForm& ShrubberyCreationForm::operator = (const ShrubberyCreationForm& assignObj)
{
    this->Form::operator=(assignObj);
    return(*this);
}

/* Methods */
void ShrubberyCreationForm::execute (Bureaucrat const & executor) const
{
    this->Form::execute(executor); // check authorizations first

    const std::string tree = "\
              v .   ._, |_  .,\n\
           `-._\\/  .  \\ /    |/_\n\
               \\\\  _\\, y | \\//\n\
         _\\_.___\\\\, \\\\/ -.\\||\n\
           `7-,--.`._||  / / ,\n\
           /'     `-. `./ / |/_.'\n\
                     |    |//\n\
                     |_    /\n\
                     |-   |\n\
                     |   =|\n\
                     |    |\n\
--------------------/ ,  . \\--------._\n\
";

    std::string const file_name = this->getTarget() + "_shrubbery";
    std::ofstream file;

    file.open(file_name.c_str());
    if (!file)
    {
        std::cerr << "Error creating file\n";
        return ;
    }
    file << tree;
    file.close();

    std::cout 
        << "\033[33mFile '"
        << file_name.c_str()
        << "' was created\033[m"
        << std::endl;
}