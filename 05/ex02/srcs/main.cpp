#include "../includes/Bureaucrat.hpp"
#include "../includes/Form.hpp"
#include "../includes/ShrubberyCreationForm.hpp"
#include "../includes/PresidentialPardonForm.hpp"
#include "../includes/RobotomyRequestForm.hpp"

int main()
{
	std::cout << "\033[31m -------- SHRUBERRY TESTS -------\033[m\n\n" << std::endl;
	std::cout << "\033[36m -------- Grade too low to be signed -------\033[m" << std::endl;
	try
	{
		ShrubberyCreationForm S("Bob");
		Bureaucrat B1("Charles",150);
		std::cout << B1 << std::endl;
		B1.signForm(S);
		S.beSigned(B1);
		B1.executeForm(S);
		std::cout << S << std::endl;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}


	std::cout << "\033[36m-------- Grade is ok, but form is not signed -------\033[m" << std::endl;
	try
	{
		ShrubberyCreationForm S1("Alice");
		Bureaucrat B2("Edouard", 2);
		std::cout << B2 << std::endl;
		B2.executeForm(S1);
		std::cout << S1 << std::endl;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}

	std::cout << "\033[36m-------- Grade is ok, form is signed ------- \033[m" << std::endl;
	try
	{
		ShrubberyCreationForm S2("Jack");
		Bureaucrat B3("Pierre", 1);
		std::cout << B3 << std::endl;
		B3.signForm(S2);
		S2.beSigned(B3);
		B3.executeForm(S2);
		std::cout << S2 << std::endl;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}

	std::cout << "\033[31m -------- PRESIDENTIAL PARDON TESTS -------\033[m\n\n" << std::endl;

	std::cout << "\033[36m -------- Grade too low to be signed -------\033[m" << std::endl;
	try
	{
		PresidentialPardonForm S("Bob");
		Bureaucrat B1("Charles",150);
		std::cout << B1 << std::endl;
		B1.signForm(S);
		S.beSigned(B1);
		B1.executeForm(S);
		std::cout << S << std::endl;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}


	std::cout << "\033[36m-------- Grade is ok, but form is not signed -------\033[m" << std::endl;
	try
	{
		PresidentialPardonForm S1("Alice");
		Bureaucrat B2("Edouard", 2);
		std::cout << B2 << std::endl;
		B2.executeForm(S1);
		std::cout << S1 << std::endl;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}

	std::cout << "\033[36m-------- Grade is ok, form is signed ------- \033[m" << std::endl;
	try
	{
		PresidentialPardonForm S2("Jack");
		Bureaucrat B3("Pierre", 1);
		std::cout << B3 << std::endl;
		B3.signForm(S2);
		S2.beSigned(B3);
		B3.executeForm(S2);
		std::cout << S2 << std::endl;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}

	std::cout << "\033[31m -------- ROBOT REQUEST TESTS -------\033[m\n\n" << std::endl;

	std::cout << "\033[36m -------- Grade too low to be signed -------\033[m" << std::endl;
	try
	{
		RobotomyRequestForm S("Bob");
		Bureaucrat B1("Charles",150);
		std::cout << B1 << std::endl;
		B1.signForm(S);
		S.beSigned(B1);
		B1.executeForm(S);
		std::cout << S << std::endl;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}


	std::cout << "\033[36m-------- Grade is ok, but form is not signed -------\033[m" << std::endl;
	try
	{
		RobotomyRequestForm S1("Alice");
		Bureaucrat B2("Edouard", 2);
		std::cout << B2 << std::endl;
		B2.executeForm(S1);
		std::cout << S1 << std::endl;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}

	std::cout << "\033[36m-------- Grade is ok, form is signed ------- \033[m" << std::endl;
	try
	{
		RobotomyRequestForm S2("Jack");
		Bureaucrat B3("Pierre", 1);
		std::cout << B3 << std::endl;
		B3.signForm(S2);
		S2.beSigned(B3);
		B3.executeForm(S2);
		std::cout << S2 << std::endl;
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	return (0);
}