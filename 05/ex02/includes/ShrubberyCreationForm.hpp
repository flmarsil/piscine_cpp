#ifndef SHRUBBERYCREATIONFORM_HPP
#define SHRUBBERYCREATIONFORM_HPP

#include "Form.hpp"

#include <iostream>
#include <fstream>

class ShrubberyCreationForm : public Form
{
    public:
        /* Constructor */
            ShrubberyCreationForm(const std::string& target);
        /* Destructor */
            virtual ~ShrubberyCreationForm();
        /* Copy constructor */
            ShrubberyCreationForm(const ShrubberyCreationForm& copyObj);
        /* Overloading operator */
            ShrubberyCreationForm& operator = (const ShrubberyCreationForm& assignObj);
        /* Methods */
            virtual void execute (Bureaucrat const & executor) const;
    
    private:
        /* Private constructor */
            ShrubberyCreationForm();
};

#endif
