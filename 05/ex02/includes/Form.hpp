#ifndef FORM_HPP
#define FORM_HPP

#include "Bureaucrat.hpp"

#include <iostream>
#include <exception>

class Bureaucrat;

class Form 
{
    public:
        /* Constructor */
            Form(const std::string& name, int gradeForSign, int gradeForExec, std::string target);
        /* Destructor */
            virtual ~Form();
        /* Copy constructor */
            Form(const Form& copyObj);
        /* Overloading operator */
            Form& operator = (const Form& assignObj);
        /* Methods */
            int getGradeForSign() const;
            int getGradeForExec() const;
            std::string getName() const;
            bool getSigned() const;
            std::string getTarget() const;
            void beSigned(const Bureaucrat& bureaucrat);
            virtual void execute (Bureaucrat const & executor) const = 0;
            // déclaré "= 0" pour forcer la mise en œuvre au niveau de la classe dérivée (la classe doit être abstraite, en fonction du sujet), mais toujours définie dans le CPP pour fournir un comportement commun

        /* Classes exceptions */
            class GradeTooHighException : public std::exception
            {
                public :
                    GradeTooHighException() throw();
                    virtual const char* what() const throw();
            };

            class GradeTooLowException : public std::exception
            {
                public :
                    GradeTooLowException() throw();
                    virtual const char* what() const throw();
            };

            class NotSignedException: public std::exception
            {
                public :
                    NotSignedException() throw();
                    virtual const char *what() const throw();
            };

    private:
            Form();
        /* Attributs */
            const std::string   _name;
            bool                _is_signed;
            const int           _gradeForSign;  // 1 =< grade <= 150
            const int           _gradeForExec;  // 1 =< grade <= 150
            const std::string   _target;
};

std::ostream& operator << (std::ostream& flux, const Form& form);

#endif
