#include "../includes/Base.hpp"

void identify_from_pointer(Base* p)
{
    if (dynamic_cast<A*>(p))
        std::cout << "A" << std::endl;
    else if (dynamic_cast<B*>(p))
        std::cout << "B" << std::endl;
    else if (dynamic_cast<C*>(p))
        std::cout << "C" << std::endl;
}

void identify_from_reference(Base& p)
{
	try
	{
		A &a = dynamic_cast<A&>(p);
		(void)a;
		std::cout << "A" << std::endl;
	}
	catch (std::exception &e) {}

	try
	{
		B &b = dynamic_cast<B&>(p);
		(void)b;
		std::cout << "B" << std::endl;
	}
	catch (std::exception &e) {}

	try
	{
		C &c = dynamic_cast<C&>(p);
		(void)c;
		std::cout << "C" << std::endl;
	}
	catch (std::exception &e) {}
}

Base* generate(void)
{
	int	nb = rand() % 3;

	if (!nb)
	{
		std::cout << "Class A created\n";
		return (new A);
	}
	else if (nb == 1)
	{
		std::cout << "Class B created\n";
		return (new B);
	}
	std::cout << "Class C created\n";
	return (new C);
}

int main()
{
	srand(time(0));
    Base*   p1 = generate();

    std::cout << "\n----- From pointer -----" << std::endl;
    
    identify_from_pointer(p1);

    std::cout << "\n----- From reference -----" << std::endl;

    identify_from_reference(*p1);
 
    delete p1;

    // system("leaks a.out");
    return (0);
}

//https://www.delftstack.com/fr/howto/cpp/dynamic-cast-cpp/