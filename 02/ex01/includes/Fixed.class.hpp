#ifndef FIXED_CLASS_HPP
#define FIXED_CLASS_HPP

#include <iostream>
#include <math.h>

class Fixed {

	public:

		Fixed(); 								// constructeur par defaut
		~ Fixed();								// destructeur
		Fixed(const Fixed& stockRef);			// constructeur de copie
		Fixed& operator=(const Fixed& copyRef); // opérateur d'assignation
		Fixed(const int n); 					// constructeur qui converti la valeur (int)n en valeur fixe
		Fixed(const float n);					// constructeur qui converti la valeur (float)n en valeur fixe
	
		int 				getRawBits(void) const;
		void 				setRawBits(int const raw);
		float 				toFloat(void) const;
		int 				toInt(void) const;

	private:

		int					_stock;		// stock la valeur a point fixe
		int static const	_nbits = 8;	// stocker le nombre de bits, toujours = à 8.
};

std::ostream& operator<<(std::ostream& oStream, const Fixed& fixed);


#endif
