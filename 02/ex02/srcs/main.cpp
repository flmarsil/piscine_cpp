#include "../includes/Fixed.class.hpp"

int main(void)
{
	// Fixed a(65);
	// Fixed b(5);
	// Fixed c;

	// c << a;
	// std::cout << "a = "<< a << std::endl;
	// std::cout << "b = "<< b << std::endl;
	// std::cout << "c = "<< c << std::endl;
	
	Fixed a;
	Fixed const b(Fixed(5.05f) * Fixed(2));
	std::cout << a << std::endl;
	std::cout << ++a << std::endl;
	std::cout << a << std::endl;
	std::cout << a++ << std::endl;
	std::cout << a << std::endl;
	std::cout << b << std::endl;
	std::cout << Fixed::max(a, b) << std::endl;
	return 0;
}
