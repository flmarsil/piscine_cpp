#include "../includes/Fixed.class.hpp"

//---------------------------------------------------
// 			CONSTRUCTEURS/DECONSTRUCTEURS 			|
//---------------------------------------------------

// constructeur par defaut
Fixed::Fixed() : _stock(0){
	std::cout << "Default constructor called\n";
	return ;
}

// constructeur de copie
Fixed::Fixed(const Fixed& stockRef) {
	std::cout << "Copy constructor called\n";
	this->_stock = stockRef.getRawBits();
	return ;
} 

// opérateur d'assignation
Fixed&
Fixed::operator=(const Fixed& copyRef){
	std::cout << "Assignation operator called\n";
	this->_stock = copyRef.getRawBits();
	return (*this);
}

// destructeur
Fixed::~ Fixed(){
	std::cout << "Destructor called\n";
	return ;
}

//---------------------------------------------------
// 						METHODES					|
//---------------------------------------------------

// initialise la valeur à point fixe
void
Fixed::setRawBits(int const raw){
	std::cout << "setRawBits member function called\n";
	this->_stock = raw;
	return ;
}

// retourne la valeur à point fixe
int
Fixed::getRawBits(void) const{
	std::cout << "getRawBits member function called\n";
	return (this->_stock);
}
