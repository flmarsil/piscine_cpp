#include "../includes/Cure.hpp"

/* Constructor  */
Cure::Cure(const std::string& type)
    : AMateria(type) {}

/* Deconstructor  */
Cure::~Cure(){}

/* Copy constructor  */
Cure::Cure(const Cure& copyObj)
    : AMateria(copyObj) {}

/* Overloading operator  */
Cure& Cure::operator = (const Cure& assignObj)
{
    if (this == &assignObj)
		return (*this);
    this->_xp = assignObj.getXP();
    return (*this);
}

/* Methods  */
void
Cure::use(ICharacter& target)
{
    std::cout
        << "* heals "
        << target.getName()
        << "’s wounds *"
        << std::endl;
    AMateria::use(target);
}

Cure* Cure::clone() const
{
    return (new Cure);
}