#include "../includes/Character.hpp"

/* Constructor  */
Character::Character(const std::string& name)
    : ICharacter(), _name(name), _nMaxMateria(4), _inventory_space(_nMaxMateria)
{
    for (int i = 0 ; i < this->_nMaxMateria ; i++)
        this->_inventory[i] = 0;
}

/* Deconstructor  */
Character::~Character()
{
    for (int idx = 0 ; idx < this->_nMaxMateria ; idx++)
        delete this->_inventory[idx];
}

/* Copy constructor  */
Character::Character(const Character& copyObj)
    : ICharacter(copyObj), _name(copyObj.getName()), _nMaxMateria(copyObj._nMaxMateria) 
{
    for (int i = 0 ; i < this->_nMaxMateria ; i++)
    {
        if (copyObj._inventory[i])
            this->_inventory[i] = copyObj._inventory[i]->clone();
        else
            this->_inventory[i] = 0;
    }
}

/* Overloading operator  */

Character& Character::operator=(Character assignObj)
{
    this->_name = assignObj.getName();
    this->_nMaxMateria = assignObj._nMaxMateria;
    this->_inventory_space = assignObj._inventory_space;

    // delete inventory if exist
    for (int i = 0 ; i < this->_nMaxMateria ; i++)
        delete this->_inventory[i];

    // copy inventory
    for (int i = 0 ; i < this->_nMaxMateria ; i++)
    {
        if (assignObj._inventory[i])
            this->_inventory[i] = assignObj._inventory[i]->clone();
        else
            this->_inventory[i] = 0;
    }
	return (*this);
}

/* Methods  */
std::string const& Character::getName() const
{
    return (this->_name);
}

void
Character::equip(AMateria* m)
{
    if (this->_inventory_space == 0)
        return;
    for (int idx = 0; idx < this->_nMaxMateria ; idx++)
    {
        if (this->_inventory[idx] == 0)
        {
            this->_inventory[idx] = m;
            break ;
        }
    }
    this->_inventory_space--;
    return ;
}

void
Character::unequip(int idx)
{
    if (idx >= this->_nMaxMateria || this->_inventory[idx] == 0)
        return ;
    this->_inventory[idx] = 0;
    this->_inventory_space++;
    return ;
}

void
Character::use(int idx, ICharacter& target)
{
    if (idx >= this->_nMaxMateria || this->_inventory[idx] == 0)
        return ;
    this->_inventory[idx]->use(target);
    return ;
}