#include "../includes/MateriaSource.hpp"

/* Constructor  */
MateriaSource::MateriaSource()
    : IMateriaSource(), _nMaxMateria(4)
{
        this->_stock_space = this->_nMaxMateria;
        for (int i = 0 ; i < this->_nMaxMateria ; i++)
            this->_stock[i] = 0;
}

/* Deconstructor  */
MateriaSource::~MateriaSource()
{
    for (int i = 0 ; i < this->_nMaxMateria ; i++)
        delete this->_stock[i];
}

/* Copy constructor  */
MateriaSource::MateriaSource (const MateriaSource & copyObj)
{
    for (int i = 0 ; i < this->_nMaxMateria ; i++)
        this->_stock[i] = copyObj._stock[i]->clone();
}

/* Overloading operator  */
MateriaSource& MateriaSource::operator=(const MateriaSource & assignObj)
{
    this->_nMaxMateria = assignObj._nMaxMateria;
    this->_stock_space = assignObj._stock_space;

    // delete stock if exist
    for (int i = 0 ; i < this->_nMaxMateria ; i++)
        delete this->_stock[i];

    // copy stock
    for (int i = 0 ; i < this->_nMaxMateria ; i++)
        this->_stock[i] = assignObj._stock[i];
    return (*this);
}

/* Methods  */
void MateriaSource::learnMateria(AMateria * m)
{
    if (this->_stock_space == 0)
        return;
    for (int idx = 0; idx < this->_nMaxMateria ; idx++)
    {
        if (this->_stock[idx] == 0)
        {
            this->_stock[idx] = m->clone();
            break ;
        }
    }
    this->_stock_space--;
    return ;   
}

AMateria* MateriaSource::createMateria(std::string const & type)
{
    if (this->_stock_space == 0)
        return (0);   
    
    int idx = 0;
    while (idx < this->_nMaxMateria && this->_stock[idx]->getType() != type)
        idx++;
    this->_stock_space++;
    return (this->_stock[idx]->clone());
}