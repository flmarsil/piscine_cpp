#ifndef CHARACTER_HPP
#define CHARACTER_HPP

#include "ICharacter.hpp"

#include <string>
#include <iostream>

class Character : public ICharacter
{
    public:
        /* Constructor  */
            Character(const std::string& name);
        /* Deconstructor  */
            virtual ~Character();
        /* Copy constructor  */
            Character(const Character& copyObj);
        /* Overloading operator  */
		    // Character& operator = (const Character& assignObj);
            Character& operator=(Character assignObj);
            friend void swap(Character& a, Character& b);
        /* Methods  */ 
            virtual std::string const & getName() const;
            virtual void equip(AMateria* m);
            virtual void unequip(int idx);
            virtual void use(int idx, ICharacter& target);

    private:
        /* Attributs */
            std::string _name;
            int         _nMaxMateria;
            int         _inventory_space;
            AMateria*   _inventory[4];

        
};

#endif
