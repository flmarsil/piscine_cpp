#ifndef AMATERIA_HPP
#define AMATERIA_HPP

#include "ICharacter.hpp" 

#include <iostream>

class ICharacter;

class AMateria
{ 
    public:
        /* Constructor  */
            AMateria(const std::string& type);
        /* Deconstructor  */
            virtual ~AMateria();
        /* Copy constructor  */
            AMateria(const AMateria& copyObj);
        /* Overloading operator  */
		    AMateria& operator=(const AMateria& assignObj);
        /* Methods  */
            unsigned int getXP() const;             //  Returns the Materia's XP
            std::string const & getType() const;    //  Returns the materia type
            virtual AMateria* clone() const = 0;
            virtual void use(ICharacter& target);

    protected:
        /* Attributs */
            unsigned int        _xp;
            std::string        _type;
};

#endif
