#ifndef ICE_HPP
#define ICE_HPP

#include <iostream>

#include "AMateria.hpp"

class Ice : public AMateria
{ 
    public:
        /* Constructor  */
            Ice(const std::string& type = "ice");
        /* Deconstructor  */
            virtual ~Ice();
        /* Copy constructor  */
            Ice(const Ice& copyObj);
        /* Overloading operator  */
		    Ice& operator=(const Ice& assignObj);
        /* Methods */
            virtual Ice* clone() const;
            virtual void use(ICharacter& target);
};

#endif
