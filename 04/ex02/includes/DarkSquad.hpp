#ifndef DARKSQUAD_HPP
#define DARKSQUAD_HPP

#include "ISquad.hpp"

class DarkSquad : public ISquad
{
    public:
        /* Constructor */
            DarkSquad();
        /* Destructor */
            virtual ~DarkSquad();
        /* Copy constructor */
            DarkSquad(const DarkSquad& assignObj);
        /* Overloading operator */
            DarkSquad& operator = (const DarkSquad& assignObj);
        /* Method */
            virtual int getCount() const;
            virtual int push(ISpaceMarine* newUnit);
            virtual ISpaceMarine* getUnit(int n) const;
    private:
        /* Attributs */
            int             _count;
            ISpaceMarine**  _unit;

};

#endif
