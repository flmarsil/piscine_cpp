#ifndef ASSAULTTERMINATOR_HPP
#define ASSAULTTERMINATOR_HPP

#include "ISpaceMarine.hpp"

#include <iostream>

class AssaultTerminator : public ISpaceMarine
{
       public:

          /* Constructor */
               AssaultTerminator();
          /* Destructor */
               virtual ~AssaultTerminator();
          /* Methods */
               virtual AssaultTerminator* clone() const;
               virtual void battleCry() const;
               virtual void rangedAttack() const;
               virtual void meleeAttack() const;
};

#endif
