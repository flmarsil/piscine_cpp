#include "../includes/Gobelin.hpp"

/* Constructor */
Gobelin::Gobelin(const std::string& name)
: Victim(name)
{
    std::cout
        << "\033[36mLe temps c'est d'l'argent !\033[m"
        << std::endl;
}

/* Destructor */
Gobelin::~Gobelin()
{
    std::cout
        << "\033[36mNiaaaark...\033[m"
        << std::endl;
}

/* Copy constructor */
Gobelin::Gobelin(const Gobelin& copyObj)
: Victim(copyObj) {}

void
Gobelin::getPolymorphed() const
{
    std::cout
        << "\033[35m"
        << this->getName()
        << " was just polymorphed into a pig!\033[m"
        << std::endl;
    return ;
}
