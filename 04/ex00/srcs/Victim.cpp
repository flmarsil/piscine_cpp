#include "../includes/Victim.hpp"

/* Constructor  */
Victim::Victim(const std::string& name)
: _name(name)
{
    if (_name != "")
        std::cout
            << "\033[36mA random victim called "
            << this->_name
            << " just appeared!\033[m"
            << std::endl;
    else
    {
        std::cout 
            << "\033[36mName or / and title of Victim forbidden :(\033[m"
            << std::endl;
    }
}

/* Destructor */
Victim::~Victim()
{
    std::cout
        << "\033[31mThe victim "
        << this->_name
        << " died for no apparent reasons!\033[m"
        << std::endl;
}

/* Copy constructor */
Victim::Victim(const Victim& copyObj)
    : _name(copyObj._name) {}


/* Methods */
const std::string& Victim::getName() const
{
	return (this->_name);
}

void
Victim::getPolymorphed() const
{
    std::cout
        << "\033[35m"
        << this->_name
        << " was just polymorphed in a cute little sheep!\033[m"
        << std::endl;
    return ;
}

/* Operation overload << */
std::ostream& operator << (std::ostream& flux, const Victim& Victim)
{
    flux 
        << "\033[32m"
        << "I am " 
        << Victim.getName() 
        << ", and I like otters!\n\033[m";
    return (flux);
}

/* Operation overload = */
Victim& Victim::operator = (const Victim& copyObj)
{
    this->_name = copyObj.getName();
    return (*this);
}
