#include "../includes/Peon.hpp"

/* Constructor */
Peon::Peon(const std::string& name)
: Victim(name)
{
    std::cout
        << "\033[36mZog zog.\033[m"
        << std::endl;
}

/* Destructor */
Peon::~Peon()
{
    std::cout
        << "\033[36mBleuark...\033[m"
        << std::endl;
}

/* Copy constructor */
Peon::Peon(const Peon& copyObj)
: Victim(copyObj) {}

void
Peon::getPolymorphed() const
{
    std::cout
        << "\033[35m"
        << this->getName()
        << " was just polymorphed into a pink pony!\033[m"
        << std::endl;
    return ;
}
