#include "../includes/Sorcerer.hpp"
#include "../includes/Victim.hpp"
#include "../includes/Peon.hpp"
#include "../includes/Gobelin.hpp"

int     main(void)
{
    Sorcerer robert("Robert", "the Magnificent");
    Victim jim("Jimmy");
    Peon joe("Joe");
    std::cout << robert << jim << joe;
    robert.polymorph(jim);
    robert.polymorph(joe);
  std::cout << "--------------------------------------" << std::endl;
    Sorcerer A("Gandalf", "Le Blanc");
    Sorcerer B("Peter", "Petit Gros");
    Sorcerer C(B);                  // test copy constructor
    B = A;                          // test overloading operator =
    std::cout << "--------------------------------------" << std::endl;
    std::cout << A << std::endl;
    std::cout << B << std::endl;    // test oberloadging operator <<
    std::cout << C << std::endl;
    std::cout << "--------------------------------------" << std::endl;
    Victim D("Victimasse");
    Victim E("Victimette");
    E = D;
    std::cout << "--------------------------------------" << std::endl;
    C.polymorph(E);
    std::cout << "--------------------------------------" << std::endl;
    Peon Z("Zordrak");
    B.polymorph(Z);
    std::cout << "--------------------------------------" << std::endl;
    Gobelin Pat("Pat");
    Gobelin Rob("Rob");
    A.polymorph(Pat);
    B.polymorph(Rob);
    std::cout << Pat << std::endl;
    std::cout << Rob << std::endl;
    std::cout << joe << std::endl;
    return (0);
}
