#ifndef GOBELIN_HPP
#define GOBELIN_HPP

#include "Victim.hpp"

class Gobelin : public Victim
{
    public:
        /* Constructor  */
        Gobelin(const std::string& name = "Pat");

        /* Destructor */
        virtual ~Gobelin();
    
        /* Copy constructor */
        Gobelin(const Gobelin&);

        /* Operation overload = */
        Gobelin& operator = (const Gobelin& assignObj);

        /* Methods */
        virtual void getPolymorphed() const;

    private:
        Gobelin();

};

#endif
