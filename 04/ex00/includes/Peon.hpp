#ifndef PEON_HPP
#define PEON_HPP

#include "Victim.hpp"

class Peon : public Victim
{
    public:
        /* Constructor  */
        Peon(const std::string& name = "Zordrak");

        /* Destructor */
        virtual ~Peon();
    
        /* Copy constructor */
        Peon(const Peon&);

        /* Operation overload = */
        Peon& operator = (const Peon& assignObj);

        /* Methods */
        virtual void getPolymorphed() const;

    private:
        Peon();

};

#endif
