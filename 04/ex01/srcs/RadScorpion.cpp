#include "../includes/RadScorpion.hpp"

/* Construcor */
RadScorpion::RadScorpion() 
    : Enemy(80, "Rad Scorpion") 
{
    std::cout
        << "* click click click *"
        << std::endl;
}

/* Destructor */
RadScorpion::~RadScorpion()
{
    std::cout
        << "* SPROTCH *"
        << std::endl;
}

/* Copy constructor */
RadScorpion::RadScorpion(const RadScorpion& copyObj)
    : Enemy(copyObj) {}

/* Operation overload  */

RadScorpion& RadScorpion::operator = (const RadScorpion& copyObj)
{
    if (this == &copyObj)
        return (*this);
    
    return (*this);
}

void
RadScorpion::takeDamage(int amount)
{
    Enemy::takeDamage(amount);
}