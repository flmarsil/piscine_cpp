#include "../includes/PowerFist.hpp"

 /* Constructor */
PowerFist::PowerFist(std::string const & name, int apcost, int damage)
    : AWeapon(name, apcost, damage) {}

/* Destructor */
PowerFist::~PowerFist() {}

/* Copy constructor */
PowerFist::PowerFist(const PowerFist& copyObj)
    : AWeapon(copyObj) {}

/* Operation overload */
PowerFist& PowerFist::operator = (const PowerFist& copyObj)
{
    if (this == &copyObj)
        return (*this);
    this->_name = copyObj.getName();
    this->_apcost = copyObj.getAPCost();
    this->_damage = copyObj.getDamage();
    return (*this);
}

/* Methods */
void PowerFist::attack() const
{
	std::cout <<  "* pschhh... SBAM ! *\n";
}
