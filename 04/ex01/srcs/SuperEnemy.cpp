#include "../includes/SuperEnemy.hpp"

/* Construcor */
SuperEnemy::SuperEnemy() 
    : Enemy(370, "Super Enemy") 
{
    std::cout
        << "BOUH !"
        << std::endl;
}

/* Destructor */
SuperEnemy::~SuperEnemy()
{
    std::cout
        << "Oooorgh ..."
        << std::endl;
}

/* Copy constructor */
SuperEnemy::SuperEnemy(const SuperEnemy& copyObj)
    : Enemy(copyObj) {}

/* Operation overload  */

SuperEnemy& SuperEnemy::operator = (const SuperEnemy& copyObj)
{
    if (this == &copyObj)
        return (*this);
    
    return (*this);
}

void
SuperEnemy::takeDamage(int amount)
{
    Enemy::takeDamage(amount - 10); // overloading resistance 
}