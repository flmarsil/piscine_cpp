#include "../includes/AWeapon.hpp"
#include "../includes/PlasmaRifle.hpp"
#include "../includes/PowerFist.hpp"
#include "../includes/Enemy.hpp"
#include "../includes/SuperMutant.hpp"
#include "../includes/RadScorpion.hpp"
#include "../includes/Character.hpp"
#include "../includes/Knife.hpp"
#include "../includes/SuperEnemy.hpp"

#include <stdio.h>

int     main()
{
    std::cout << "-------------------" << std::endl;
    PlasmaRifle H("AK-47", 40, 20);
    std::cout << H.getName() << std::endl;

    Enemy Paysan(100, "paysan");

    Character Peter("Peter");
    Peter.equip(&H);
    std::cout << Peter << std::endl;
    std::cout << Peter.getApoint() << std::endl;

    Peter.attack(&Paysan);
    std::cout << Peter.getApoint() << std::endl;
    std::cout << "-------------------" << std::endl;

    PowerFist P;
    std::cout << P.getName() << std::endl;
    P.attack();
    std::cout << "-------------------" << std::endl;

    Knife K("Couteau Suisse", 5, 10);
    K.attack();
    SuperEnemy Boss;
    Peter.equip(&K);
    Peter.attack(&Boss);
    Peter.recoverAP();
    Peter.recoverAP();
    Peter.recoverAP();
    Peter.attack(&Boss);


    std::cout << "-------------------" << std::endl;

    Enemy E(100, "Rools");

    std::cout 
        << E.getHP() << std::endl;
    E.takeDamage(55);
    std::cout 
        << E.getHP() << std::endl;

    std::cout << "-------------------" << std::endl;
    SuperMutant Mutant;
    std::cout
        << Mutant.getType()
        << " "
        << Mutant.getHP()
        << std::endl;
    Mutant.takeDamage(70);
    std::cout
        << Mutant.getType()
        << " "
        << Mutant.getHP()
        << std::endl;
    std::cout << "-------------------" << std::endl;

    RadScorpion Scorbut;
    std::cout
        << Scorbut.getType()
        << " "
        << Scorbut.getHP()
        << std::endl;
    Scorbut.takeDamage(70);
    std::cout
        << Scorbut.getType()
        << " "
        << Scorbut.getHP()
        << std::endl;
    std::cout << "-------------------" << std::endl;

    Character Hero("Trump");
    std::cout
        << Hero.getName()
        << "\n"
        << Hero.getApoint()
        << "\n"
        << Hero.getWeapon()
        << std::endl;
    
    return (0);
}


// int     main()
// {
//     Character* moi = new Character("moi");
//        std::cout << *moi;
//     Enemy* b = new RadScorpion();
//     AWeapon* pr = new PlasmaRifle();
//     AWeapon* pf = new PowerFist();
//     moi->equip(pr);
//     std::cout << *moi;
//     moi->equip(pf);
//     moi->attack(b);
//     std::cout << *moi;
//     moi->equip(pr);
//     std::cout << *moi;
//     moi->attack(b);
//     std::cout << *moi;
//     moi->attack(b);
//        std::cout << *moi;
//     return (0);
// }