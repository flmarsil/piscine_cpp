#ifndef SUPERENEMY_HPP
#define SUPERENEMY_HPP

#include "Enemy.hpp"

#include <iostream>
#include <string>

class SuperEnemy : public Enemy
{
    public:
        /* Construcor */
            SuperEnemy();
        /* Destructor */
            virtual ~SuperEnemy();
        /* Copy constructor */
            SuperEnemy(const SuperEnemy& copyObj);
        /* Operation overload  */
            SuperEnemy& operator = (const SuperEnemy& copyObj);
        /* Methods */
            virtual void takeDamage(int amount);
};

#endif
