#ifndef PLASMARIFLE_HPP
#define PLASMARIFLE_HPP

#include "AWeapon.hpp"

class PlasmaRifle : public AWeapon
{
    public:
        /* Constructor */
            PlasmaRifle(std::string const & name = "Plasma Rifle", int apcost = 5, int damage = 21);
        /* Destructor */
            virtual ~PlasmaRifle();
        /* Copy constructor */
            PlasmaRifle(const PlasmaRifle& copyObj);
        /* Operation overload */
            PlasmaRifle& operator = (const PlasmaRifle& other);
        /* Methods */
            virtual void attack() const;
};

#endif
