#ifndef KNIFE_HPP
#define KNIFE_HPP

#include "AWeapon.hpp"

class Knife : public AWeapon
{
    public:
        /* Constructor */
            Knife(std::string const & name, int apcost, int damage);
        /* Destructor */
            virtual ~Knife();
        /* Copy constructor */
            Knife(const Knife& copyObj);
        /* Operation overload */
            Knife& operator = (const Knife& copyObj);
        /* Methods */
            virtual void attack() const;
};

#endif
