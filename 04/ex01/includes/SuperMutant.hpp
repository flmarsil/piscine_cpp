#ifndef SUPERMUTANT_HPP
# define SUPERMUTANT_HPP

#include "../includes/Enemy.hpp"

class SuperMutant : public Enemy
{   
    public:
        /* Construcor */
            SuperMutant();
        /* Destructor */
            virtual ~SuperMutant();
        /* Copy constructor */
            SuperMutant(const SuperMutant& copyObj);
        /* Operation overload  */
            SuperMutant& operator = (const SuperMutant& copyObj);
        /* Methods */
            virtual void takeDamage(int amount);
};

#endif
