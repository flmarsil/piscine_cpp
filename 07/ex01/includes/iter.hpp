#ifndef ITER_HPP
#define ITER_HPP

#include <iostream>

template <typename T>
void iter (const T* array, size_t size, void (*p_function)(const T&))
{
    for (size_t i = 0 ; i < size ; i++)
        p_function(array[i]);
}

#endif
