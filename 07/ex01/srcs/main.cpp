#include "../includes/iter.hpp"

template <typename T>
void print_thing(const T& input)
{
    std::cout << input << std::endl;
}

int main()
{
    const size_t size = 6;

    std::cout << std::endl;
    std::cout << "*********** INT ARRAY TEST ***********" << std::endl;
    
    int int_array[size] = {56, 45, 32, 64, 32, 65};
    iter(int_array, size , &print_thing);

    std::cout << std::endl;
    std::cout << "*********** FLOAT ARRAY TEST ***********" << std::endl;
    
    float float_array[size] = {34.342, 412.323, 32.43, 545.64, 65.32, 343.65};
    iter(float_array, size , &print_thing);

    std::cout << std::endl;
    std::cout << "*********** CHAR ARRAY TEST ***********" << std::endl;

    char char_array[size] = {'a', 'b', 'c', 'd', 'e', 'f'};
    iter(char_array, size, &print_thing);

    std::cout << std::endl;
    std::cout << "*********** STRING ARRAY TEST ***********" << std::endl;

    char string_array[size][10] = {"Hello", "World", "it", "is", "me", "Flmarsil"};
    iter(string_array, size, &print_thing);
}
