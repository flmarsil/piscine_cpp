#ifndef WHATEVER_HPP
#define WHATEVER_HPP

#include <iostream>

template<typename T>
void swap(T& a, T& b)
{
    T tmp = a; // T tmp(a); <- use this one for swap classes with no default constructor
    a = b;
    b = tmp;
}

template<typename T>
T const &min(const T &a, const T& b)
{
    return ((a >= b) ? b : a);
}

template<typename T>
T const &max(const T &a, const T &b)
{
    return ((a <= b) ? b : a);
}

#endif
