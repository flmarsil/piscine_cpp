#include "../includes/whatever.hpp"

// int main()
// {
//     int a = 56;
//     int b = 32;

//     char c = 'f';
//     char d = 'g';

//     float e = 56.43;
//     float f = 33.333;

//     std::cout << "\n************ TEST SWAP ************* \n";

//     std::cout << "\n------ Before Swap ------ \n";
//     std::cout << "int a = " << a << std::endl;
//     std::cout << "int b = " << b << std::endl;
//     std::cout << std::endl;
//     std::cout << "char c = " << c << std::endl;
//     std::cout << "char d = " << d << std::endl;
//     std::cout << std::endl;
//     std::cout << "float e = " << e << std::endl;
//     std::cout << "float f = " << f << std::endl;

//     swap(a, b);
//     swap(c, d);
//     swap(e, f);

//     std::cout << "\n------ After Swap ------ \n";
//     std::cout << "int a = " << a << std::endl;
//     std::cout << "int b = " << b << std::endl;
//     std::cout << std::endl;
//     std::cout << "char c = " << c << std::endl;
//     std::cout << "char d = " << d << std::endl;
//     std::cout << std::endl;
//     std::cout << "float e = " << e << std::endl;
//     std::cout << "float f = " << f << std::endl;

//     std::cout << "\n************ TEST MIN ************* \n";

//     int ret = min(a, b);
//     char ret3 = min(c, d);
//     float ret2 = min(e, f);

//     std::cout << "int (a - b) min = " << ret << std::endl;
//     std::cout << "char (c - d) min = " << ret3 << std::endl;
//     std::cout << "float (e - f) min = " << ret2 << std::endl;

//    std::cout << "\n************ TEST MAX ************* \n";

//     int ret4 = max(a, b);
//     char ret6 = max(c, d);
//     float ret5 = max(e, f);
    
//     std::cout << "int (a - b) max = " << ret4 << std::endl;
//     std::cout << "char (c - d) max = " << ret6 << std::endl;
//     std::cout << "float (e - f) max = " << ret5 << std::endl;

//     return (0);
// }

#include "../includes/whatever.hpp"

void    swap_test()
{
    int a = 5;
    int b = 7;
    std::cout << "value1 : " << a << " / value2 : " << b << "\n";
    swap(a, b);
    std::cout << "swap\n";
    std::cout << "value1 : " << a << " / value2 : " << b << "\n\n";

    char c = 'e';
    char d = 'h';
    std::cout << "value1 : " << c << " / value2 : " << d << "\n";
    swap(c, d);
    std::cout << "swap\n";
    std::cout << "value1 : " << c << " / value2 : " << d << "\n\n";

    std::string e = "swap";
    std::string f = "paws";
    std::cout << "value1 : " << e << " / value2 : " << f << "\n";
    ::swap(e, f);
    std::cout << "swap\n";
    std::cout << "value1 : " << e << " / value2 : " << f << "\n\n";

    float g = 12.5f;
    float h = 12800.3f;
    std::cout << "value1 : " << g << " / value2 : " << h << "\n";
    swap(g, h);
    std::cout << "swap\n";
    std::cout << "value1 : " << g << " / value2 : " << h << "\n\n";
}

void    min_test()
{
    const int a = 5;
    const int b = 7;
    std::cout << "value1 : " << a << " / value2 : " << b << "\n";
    std::cout << "min\n";
    std::cout << "min value is : " << min(a, b) << "\n";
    std::cout << "min value is : " << min(b, a) << "\n\n";

    char c = 'e';
    char d = 'h';
    std::cout << "value1 : " << c << " / value2 : " << d << "\n";
    std::cout << "min\n";
    std::cout << "min value is : " << min(c, d) << "\n";
    std::cout << "min value is : " << min(d, c) << "\n\n";

    std::string const e = "min";
    std::string const f = "paws";
    std::cout << "value1 : " << e << " / value2 : " << f << "\n";
    std::cout << "min\n";
    std::cout << "min value is : " << ::min(e, f) << "\n";
    std::cout << "min value is : " << ::min(f, e) << "\n\n";

    float g = 12.5f;
    float h = 12800.3f;
    std::cout << "value1 : " << g << " / value2 : " << h << "\n";
    std::cout << "min\n";
    std::cout << "min value is : " << min(g, h) << "\n";
    std::cout << "min value is : " << min(h, g) << "\n\n";

    std::cout << "with equal value : " << min(static_cast<int>(12), static_cast<int>(12)) << std::cout << "\n";
}

void    max_test()
{
    const int a = 5;
    const int b = 7;
    std::cout << "value1 : " << a << " / value2 : " << b << "\n";
    std::cout << "max\n";
    std::cout << "max value is : " << max(a, b) << "\n";
    std::cout << "max value is : " << max(b, a) << "\n\n";

    char c = 'e';
    char d = 'h';
    std::cout << "value1 : " << c << " / value2 : " << d << "\n";
    std::cout << "max\n";
    std::cout << "max value is : " << max(c, d) << "\n";
    std::cout << "max value is : " << max(d, c) << "\n\n";

    std::string e = "max";
    std::string f = "paws";
    std::cout << "value1 : " << e << " / value2 : " << f << "\n";
    std::cout << "max\n";
    std::cout << "max value is : " << ::max(e, f) << "\n";
    std::cout << "max value is : " << ::max(f, e) << "\n\n";

    float const g = 12.5f;
    float const h = 12800.3f;
    std::cout << "value1 : " << g << " / value2 : " << h << "\n";
    std::cout << "max\n";
    std::cout << "max value is : " << max(g, h) << "\n";
    std::cout << "max value is : " << max(h, g) << "\n\n";

	std::cout << "value1 : " << 1 << " / value2 : " << 2 << "\n";
    std::cout << "max value is : " << max(1, 2) << "\n";

}

void    subject_test()
{
    int a = 2;
    int b = 3;
    ::swap( a, b );
    std::cout << "a = " << a << ", b = " << b << std::endl;
    std::cout << "min( a, b ) = " << ::min( a, b ) << std::endl;
    std::cout << "max( a, b ) = " << ::max( a, b ) << std::endl;
    std::string c = "chaine1";
    std::string d = "chaine2";
    ::swap(c, d);std::cout << "c = " << c << ", d = " << d << std::endl;
    std::cout << "min( c, d ) = " << ::min( c, d ) << std::endl;
    std::cout << "max( c, d ) = " << ::max( c, d ) << std::endl;
}

int main()
{
    std::cout << "SWAP TEST :\n";
    swap_test();
    std::cout << "\nMIN TEST\n";
    min_test();
    std::cout << "\nMAX TEST\n";
    max_test();
    std::cout << "\nSUBJECT TEST\n";
    subject_test();

    return (0);
}