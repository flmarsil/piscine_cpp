/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/22 19:00:32 by a42               #+#    #+#             */
/*   Updated: 2021/01/06 16:26:37 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

Pony::Pony(){
	this->exist = 1;
	this->_kmh = 375;
	std::cout << "\033[36mConstructeur : Pony (\033[m" << this << "\033[36m) : construit\033[m" << std::endl;
	return ;
}

Pony::~ Pony(){
	if (this->exist == 1)
	{
		std::cout << "\033[31mDestructeur : Pony (\033[m" << this << "\033[31m) : détruit\033[m" << std::endl;
		this->exist = 0;
	}
	return ;
}

void
Pony::ponyOnTheStack(){
	std::cout << "---------------------------------------------" << std::endl;
	std::cout << "   🎠 PONY STACK IS RUNNING ON THE STACK 🎠"   << std::endl;
	std::cout << "---------------------------------------------" << std::endl;
	std::cout << "Il court actuellement à " << this->_kmh << " km/heure !\n" << std::endl;
	this->~ Pony();
	return ;
}

void
Pony::ponyOnTheHeap(){
	std::cout << "---------------------------------------------" << std::endl;
	std::cout << "    🏇 PONY HEAP IS RUNNING ON THE HEAP 🏇"     << std::endl;
	std::cout << "---------------------------------------------" << std::endl;
	std::cout << "Il court actuellement à " << this->_kmh << " km/heure\n" << std::endl;
	std::cout << "\033[35mDelete (free): Pony (\033[m" << this << "\033[35m) : deleted\033[m" << std::endl;
	delete this;
	this->exist = 0;
	return ;
}
