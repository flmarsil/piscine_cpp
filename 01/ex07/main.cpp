#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>

// vérification des inputs
bool 	check_error(int ac, char **av)
{
	if (ac != 4){
		std::cout << "Erreur : veuillez rentrer 3 arguments\n";
		return (false);
	}
	if (!av[1][0] || !av[2][0] || !av[3][0]){
		std::cout << "Erreur : les arguments ne doivent pas êtres vides\n";
		return (false);
	}
	return (true);
}

int		main(int ac, char **av)
{
	if (check_error(ac, av) == false)
		return (1);
	
	std::string file(av[1]), s1(av[2]), s2(av[3]), buf, newFile;

	// test pour verifier que l'occurence recherchée existe dans le fichier
	std::stringstream test;
	std::ifstream testFile(file.c_str());
	test << testFile.rdbuf();
	buf = test.str();
	std::size_t found = buf.find(s1); // find renvoit l'emplacement dans la chaine où l'occurence correspond sinon renvoit std::string::npos
	testFile.close();
	if (found == std::string::npos){
		std::cout << "Erreur : Aucune occurrence correspondante n'a été trouvé dans le fichier, aucun remplacement n'a été effectué\n";
		return (1);
	}

	// vérification de l'existance du fichier + ouverture
	std::ifstream readFile(file.c_str());
	if (!readFile){
		std::cout << "Erreur : l'ouverture du fichier a écouché\n";
		return (1);
	}

	// création du nouveau fichier qui recevra les chaines modifiés
	newFile = file + ".replace";
	std::ofstream output(newFile.c_str());
	if (!output){
		std::cout << "Erreur : création du nouveau fichier a écouché\n";
		return (1);
	}

	// boucle de remplacement
	while (std::getline(readFile, buf))
	{
		while (buf.find(s1) != std::string::npos)
			buf.replace(buf.find(s1), s1.length(), s2);
		output << buf << std::endl;
	}
	readFile.close();
	output.close();
	return (0);
}
	