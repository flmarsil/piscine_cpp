#ifndef ZOMBIEHORDE_HPP
#define ZOMBIEHORDE_HPP

#include "../includes/Zombie.hpp"

class ZombieHorde{

	public:

		ZombieHorde(unsigned int n);
		~ ZombieHorde();

		void	announce() const;

	private:

		int			_nZomb;
		Zombie*		_tabZomb;
		std::string _listNames[5]; //tableau de noms aléatoires
};

#endif
