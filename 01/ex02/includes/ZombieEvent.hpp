/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/25 11:55:17 by a42               #+#    #+#             */
/*   Updated: 2020/12/28 11:32:21 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIEEVENT_HPP
#define ZOMBIEEVENT_HPP

#include "Zombie.hpp"
#include <cstdlib>

class ZombieEvent{

	public:

		ZombieEvent();
		ZombieEvent(std::string type);
		~ ZombieEvent();

		void	setZombieType(std::string type);
		Zombie* newZombie(std::string name);
		Zombie* randomChump() const;

	private:
	
		std::string _type;
		std::string _listNames[5]; //tableau de noms aléatoires
};

#endif