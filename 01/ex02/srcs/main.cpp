/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/25 11:33:58 by a42               #+#    #+#             */
/*   Updated: 2021/01/03 14:09:52 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/Zombie.hpp"
#include "../includes/ZombieEvent.hpp"

void	zombie_stack_creation()
{
	std::cout << "\033[32m\n\n🧟‍♀️ Un zombie apparait sur la stack 🧟‍♀️\n\n\033[m" << std::endl;
	Zombie stack;
	stack.advert();
	std::cout << "\nVous attaquez Jack et lui portez un coup fatal ! 💥💥💥\n";
	std::cout << "Jack meurt 💀\n" << std::endl;
	return ;
}

void 	zombie_heap_creation()
{
	std::cout << "\n\nContinuons notre chemin, vite courrez ! 🏃‍♂️🏃‍♂️🏃‍♂️\n";
	std::cout << "\nAttendez ! Vous avez entendu ? Quel est ce bruit ? ATTENTION 😱😱😱!" << std::endl;
	std::cout << "\033[32m\n🧟🧟🧟 Trois clones zombies apparaissent sur le Heap 🧟🧟🧟\n\033[m" << std::endl;
	ZombieEvent heapEvent;
	Zombie *heapZombie1, *heapZombie2, *heapZombie3;
	heapZombie1 = heapEvent.randomChump();
	heapZombie2 = heapEvent.randomChump();
	heapZombie3 = heapEvent.randomChump();
	std::cout << "\nVous attaquez le groupe de zombies avec votre super attaque ! 🔥 ☄️ ⚡️ ⛈ ❄️ \n";
	std::cout << "Tous les zombies meurt 💀\n" << std::endl;
	delete heapZombie1;
	delete heapZombie2;
	delete heapZombie3;
	return ;
}

int		main()
{
	int				stop;
	std::string 	buf;

	std::cout << "---------------------------------------------" << std::endl;
	std::cout << "                ZOMBIELAND"                  << std::endl;
	std::cout << "---------------------------------------------" << std::endl;
	std::cout << "Un virus s'est propagé sur Terre, le Code-vide-42,\nLes scientifiques ont élaboré un vaccin." << std::endl;
	std::cout << "Malheureusement les choses ont mal tourné ..." << std::endl;
	std::cout << "Il a transformé une partie de la population en choses étranges." << std::endl;
	std::cout << "Vous devez trouver et tuer leur chef Groszomb afin que nous fabriquions l'antidote.\n" << std::endl;
	std::cout << "Choisissez votre zéro : " << std::endl;
	std::cout << "1 - 🧙‍♂️ Big magozer \n2 - 🧝‍♀️ Elfette de la night\n3 - 🥷  Ninja scred" << std::endl;
	stop = 1;
	while (stop > 0)
	{
		std::cout << "$";
		std::getline(std::cin, buf);
		buf.erase(std::remove_if(buf.begin(), buf.end(), ::isspace), buf.end());
		(atoi(buf.c_str()) == 1) ? std::cout << "Ok Big magozer, let's go !" << std::endl : 0;
		(atoi(buf.c_str()) == 2) ? std::cout << "Ok Elfette de la night, let's go !" << std::endl : 0;
		(atoi(buf.c_str()) == 3) ? std::cout << "Ok Ninja scred, let's go !" << std::endl : 0;
		zombie_stack_creation();
		zombie_heap_creation();
		stop--;
	}
	system("leaks Zombie");
	return (0);
}
