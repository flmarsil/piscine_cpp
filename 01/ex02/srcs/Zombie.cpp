/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/24 16:27:08 by a42               #+#    #+#             */
/*   Updated: 2020/12/28 11:28:31 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/Zombie.hpp"

//constructeur par defaut
Zombie::Zombie() : _name("Jack"), _type("Zombie stack"){}

Zombie::~ Zombie(){
	std::cout << "\033[31mDestructeur : Zombie (\033[m" << this << "\033[31m) : détruit\033[m" << std::endl;
	return ;
}

//constructeur avec nom et type ZombieEvent
Zombie::Zombie(std::string name, std::string type) : _name(name), _type(type){}

//affiche non et type du zombie
void
Zombie::advert() const{
	std::cout << "<" << this->_name << " (" << this->_type << ")" << "> Braiiiiiiinnnssss ..." << std::endl;
	return ;
}

// //retourne le nom du zombie
// std::string
// Zombie::getVarName() const{
// 	return (this->_name);
// }
