#include "../includes/Brain.hpp"

Brain::Brain(){}
Brain::~ Brain(){}

std::string
Brain::identifier() const{
	std::string addr;
	std::stringstream str;

	str << this;
	addr = str.str();
	for (int i = 2; addr[i] ; i++)
		addr[i] = toupper(addr[i]);
	return(addr);
}
