#ifndef Human_HPP
#define Human_HPP

#include <iostream>
#include "Brain.hpp"

class Human {

	public:

		Human();
		~ Human();
		std::string	identifier() const;
		const Brain&	getBrain() const;

	private:

		const Brain _brain;

};

#endif
