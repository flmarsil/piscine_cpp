#ifndef MUTANTSTACK_HPP
#define MUTANTSTACK_HPP

#include <iostream>
#include <stack>

template <typename T>
class MutantStack : public std::stack<T>
{
    public:
        /* Constructor */
            MutantStack();
        /* Destructor */
            ~MutantStack();
        /* Copy constructor */
            MutantStack(const MutantStack& copyObj);
        /* Overloading operator */
           MutantStack<T>& operator = (const MutantStack<T>& assignObj);
        /* Iterator */
        typedef typename std::stack<T>::container_type::iterator iterator;
        /* Overloading methods */
        iterator begin(void);
        iterator end(void);
};

/* Constructor */
template <typename T>
MutantStack<T>::MutantStack() : std::stack<T>() {}

/* Destructor */
template <typename T>
MutantStack<T>::~MutantStack() {}

/* Copy constructor */
template <typename T>
MutantStack<T>::MutantStack(const MutantStack& copyObj) : std::stack<T>(copyObj) {}

/* Overloading operator */
template <typename T>
MutantStack<T>& MutantStack<T>::operator = (const MutantStack<T>& assignObj)
{
    std::stack<T>::operator = (assignObj);
    return (*this);
}

/* Overloading methods */
template <typename T>
typename MutantStack<T>::iterator MutantStack<T>::end(void)
{
    return (std::stack<T>::c.end());
}

template <typename T>
typename MutantStack<T>::iterator MutantStack<T>::begin(void)
{
    return (std::stack<T>::c.begin());
}

#endif
