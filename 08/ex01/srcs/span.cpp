#include "../includes/span.hpp"

/* Constructor */
Span::Span(unsigned int input) : _storageCapacity(input) {}

/* Destructor */
Span::~Span() {}

/* Copy constructor */
Span::Span(const Span& copyObj)
    : _storageCapacity(copyObj._storageCapacity), _List(copyObj._List) {}

/* Overloading operator */
Span& Span::operator = (const Span& assignObj)
{
    this->_storageCapacity = assignObj._storageCapacity;
    this->_List = assignObj._List;
    return (*this);
}

/* Methods */
void Span::addNumber(unsigned int number)
{
    (this->_List.size() >= this->_storageCapacity)
        ? throw std::exception()
        : this->_List.push_back(number);
}

int Span::shortestSpan() const
{
    (this->_List.size() < 2)
        ? throw std::exception() : 0;

    std::vector<int> tmp = this->_List;

    std::sort(tmp.begin(), tmp.end());

    int min = *(tmp.begin());
    int max = *(tmp.begin() + 1);

    if (min == max)
    {
        std::vector<int>::iterator it;
        for (it = tmp.begin() ; it != tmp.end() ; ++it)
        {
            if (*it != *(it + 1))
            {
                max = *(it + 1);
                break ;
            }
        }
    }
    return (max - min);
}

int Span::longestSpan() const
{
    (this->_List.size() < 2)
        ? throw std::exception() : 0;
    
    std::vector<int> tmp = this->_List;

    int min = *std::min_element(tmp.begin(), tmp.end());
    int max = *std::max_element(tmp.begin(), tmp.end());
    return (max - min);
}

std::vector<int>& Span::getVector()
{
    return (this->_List);
}

unsigned int Span::getStorage() const
{
    return (this->_storageCapacity);
}
