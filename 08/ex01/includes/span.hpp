#ifndef SPAN_HPP
#define SPAN_HPP

#include <iostream>
#include <vector>
#include <exception>
#include <algorithm>

class Span 
{
    public:
        /* Constructor */
            Span(unsigned int input);
        /* Destructor */
            ~Span();
        /* Copy constructor */
            Span(const Span& copyObj);
        /* Overloading operator */
            Span& operator = (const Span& assignObj);
        /* Methods */
            void addNumber(unsigned int number);
            int shortestSpan() const;
            int longestSpan() const;
            unsigned int getStorage() const;
            std::vector<int>& getVector();

    private:
            Span();
        /* Attributs */
            unsigned int        _storageCapacity;
            std::vector<int>    _List;
};

#endif
