/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contacts.class.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 01:09:42 by a42               #+#    #+#             */
/*   Updated: 2020/12/21 20:27:08 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONTACTS_CLASS_HPP
# define CONTACTS_CLASS_HPP

class Contacts {

public:
	Contacts(void); 		// constructeur
	~ Contacts(void);		// destructeur

	void		func_firstname(int option);
	void		func_lastname(int option);
	void		func_nickname(int option);
	void		func_login(int option);
	void		func_postaladdress(int option);
	void		func_emailaddress(int option);
	void		func_phonenumber(int option);
	void		func_birthdaydate(int option);
	void		func_favoritemeal(int option);
	void		func_underwearcolor(int option);
	void		func_darkestsecret(int option);
	std::string get_privatevar(int option);

	//fonction get;
	//fonction set;

private:
	std::string _firstName;
	std::string _lastName;
	std::string _nickName;
	std::string _login;
	std::string _postalAddress;
	std::string _emailAddress;
	std::string _phoneNumber;
	std::string _birthdayDate;
	std::string _favoriteMeal;
	std::string _underwearColor;
	std::string _darkestSecret;
};

# endif
