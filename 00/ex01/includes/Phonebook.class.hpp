/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Phonebook.class.hpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/11 10:25:54 by a42               #+#    #+#             */
/*   Updated: 2020/12/21 21:04:22 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHONEBOOK_CLASS_HPP
# define PHONEBOOK_CLASS_HPP

#include <iostream>
#include <cstring>
#include <limits>
#include <algorithm>
#include <string>
#include <iomanip>
#include "../includes/Contacts.class.hpp"

class Phonebook {
	
public:
	Phonebook(void); 					// constructeur
	~ Phonebook(void);					// destructeur
	int 			n_reg;
	Contacts		book[9];

	void 			func_boucle(void);
	void 			func_add(void);
	void 			func_search(int option);
	void 			func_exit(void) const;
	void			func_print(int option);
	std::string 	stringResize(std::string &s) const;
};

# endif






/*
	INPUT :
		- ADD
		- SEARCH
		- EXIT
	
	Contact =



	Demarrage du programme : demande d'input
	Une fonction qui récupère l'argument et redirige vers fonction 
*/