/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contacts.class.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 01:13:01 by a42               #+#    #+#             */
/*   Updated: 2020/12/21 20:28:27 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/Phonebook.class.hpp"
#include "../includes/Contacts.class.hpp"

Contacts::Contacts(void){
	return ;
}

Contacts::~ Contacts(void){
	return ;
}

std::string
Contacts::get_privatevar(int option)
{
	if (option == 0)
		return (this->_firstName);
	else if (option == 1)
		return (this->_lastName);
	else if (option == 2)
		return (this->_nickName);
	else
		return (NULL);
}

void
Contacts::func_firstname(int option){
	std::string		buf;
	std::cout << "First name : ";
	if (!option)
	{
		std::getline(std::cin, buf);
		this->_firstName = buf;
		std::cin.clear();
	}
	else
		std::cout << this->_firstName << std::endl;
	return ;
}

void
Contacts::func_lastname(int option){
	std::string		buf;
	std::cout << "Last name : ";
	if (!option)
	{
		std::getline(std::cin, buf);
		this->_lastName = buf;
		std::cin.clear();
	}
	else
		std::cout << this->_lastName << std::endl;
	return ;
}

void
Contacts::func_nickname(int option){
	std::string		buf;
	std::cout << "Nickname : ";
	if (!option)
	{
		std::getline(std::cin, buf);
		this->_nickName = buf;
		std::cin.clear();
	}
	else
		std::cout << this->_nickName << std::endl;
	return ;
}

void
Contacts::func_login(int option){
	std::string		buf;
	std::cout << "Login : ";
	if (!option)
	{
		std::getline(std::cin, buf);
		this->_login = buf;
		std::cin.clear();
	}
	else
		std::cout << this->_login << std::endl;
	return ;
}

void
Contacts::func_postaladdress(int option){
	std::string		buf;
	std::cout << "Postal adress : ";
	if (!option)
	{
		std::getline(std::cin, buf);
		this->_postalAddress = buf;
		std::cin.clear();
	}
	else
		std::cout << this->_postalAddress << std::endl;
	return ;
}

void
Contacts::func_emailaddress(int option){
	std::string		buf;
	std::cout << "Email adress : ";
	if (!option)
	{
		std::getline(std::cin, buf);
		this->_emailAddress = buf;
		std::cin.clear();
	}
	else
		std::cout << this->_emailAddress << std::endl;
	return ;
}

void
Contacts::func_phonenumber(int option){
	std::string		buf;
	std::cout << "Phone number : ";
	if (!option)
	{
		std::getline(std::cin, buf);
		this->_phoneNumber = buf;
		std::cin.clear();
	}
	else
		std::cout << this->_phoneNumber << std::endl;
	return ;
}

void
Contacts::func_birthdaydate(int option){
	std::string		buf;
	std::cout << "Birthday Date : ";
	if (!option)
	{
		std::getline(std::cin, buf);
		this->_birthdayDate = buf;
		std::cin.clear();
	}
	else
		std::cout << this->_birthdayDate << std::endl;
	return ;
}

void
Contacts::func_favoritemeal(int option){
	std::string		buf;
	std::cout << "Favorite meal : ";
	if (!option)
	{
		std::getline(std::cin, buf);
		this->_favoriteMeal = buf;
		std::cin.clear();
	}
	else
		std::cout << this->_favoriteMeal << std::endl;
	return ;
}

void
Contacts::func_underwearcolor(int option){
	std::string		buf;
	std::cout << "Underwear color : ";
	if (!option)
	{
		std::getline(std::cin, buf);
		this->_underwearColor = buf;
		std::cin.clear();
	}
	else
		std::cout << this->_underwearColor << std::endl;
	return ;
}

void
Contacts::func_darkestsecret(int option){
	std::string		buf;
	std::cout << "Darkest secret : ";
	if (!option)
	{
		std::getline(std::cin, buf);
		this->_darkestSecret = buf;
		std::cin.clear();
	}
	else
		std::cout << this->_darkestSecret << "\n" << std::endl;
	return ;
}
