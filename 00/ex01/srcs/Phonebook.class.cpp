/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Phonebook.class.cpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/11 12:17:29 by a42               #+#    #+#             */
/*   Updated: 2020/12/21 21:21:36 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/Phonebook.class.hpp"
#include "../includes/Contacts.class.hpp"

Phonebook::Phonebook(void){
	this->n_reg = 0;
	return ;
}

Phonebook::~ Phonebook(void){
	return ;
}

std::string
Phonebook::stringResize(std::string &s) const{
	if (s.length() > 10)
		return (s.substr(0, 9) + ".");
	else
		return (s);
}

void
Phonebook::func_print(int option){

	std::string line;
	if (option == 0){
		std::cout << "---------------------------------------------" << std::endl;
		std::cout << "     📞 BIENVENUE SUR VOTRE PHONEBOOK 📞"     << std::endl;
		std::cout << "  Commandes disponibles : ADD, SEARCH, EXIT" << std::endl;
		std::cout << "---------------------------------------------" << std::endl;
	}
	else if (option == 1){
		std::cout << "---------------------------------------------" << std::endl;
		std::cout << "            ➕ AJOUTER UN CONTACT"             << std::endl;
		std::cout << "---------------------------------------------" << std::endl;
	}
	else if (option == 2){
		std::cout << "---------------------------------------------" << std::endl;
		std::cout << "             RÉPERTOIRE VIDE"                << std::endl;
		std::cout << "---------------------------------------------" << std::endl;
	}
	else if (option == 3){
		std::cout << "---------------------------------------------" << std::endl;
		std::cout << "             🔍 CONTACT"                       << std::endl;
		line.append(10 * 4 + 5, '-');
		std::cout << line << std::endl;
		std::cout << "|" << std::setw(10) << "     index" << "|";
		std::cout << std::setw(10) << "first name" << "|";
		std::cout << std::setw(10) << " last name" << "|";
		std::cout << std::setw(10) << "  nickname" << "|" << std::endl;
		std::cout << line << std::endl;
		for (int i = 0 ; i < this->n_reg ; i++){
		std::string item;
		std::cout << "|" << std::setw(10) << i + 1;
		item = this->book[i].get_privatevar(0);
		std::cout << "|" << std::setw(10) << this->stringResize(item);
		item = this->book[i].get_privatevar(1);
		std::cout << "|" << std::setw(10) << this->stringResize(item);
		item = this->book[i].get_privatevar(2);
		std::cout << "|" << std::setw(10) << this->stringResize(item);
		std::cout << "|" << std::endl;
		std::cout << line << std::endl;
		}
		std::cout << "\n" << "✅ Choisissez l'index du contact à afficher : " ;
	}

	return ;
}

void
Phonebook::func_add(void){
	Contacts 	add;

	this->func_print(1);
	(this->n_reg == 9) ? this->n_reg = 0 : 0;	//écrasement contact si répertoire plein
	add.func_firstname(0);
	add.func_lastname(0);
	add.func_nickname(0);
	add.func_login(0);
	add.func_postaladdress(0);
	add.func_emailaddress(0);
	add.func_phonenumber(0);
	add.func_birthdaydate(0);
	add.func_favoritemeal(0);
	add.func_underwearcolor(0);
	add.func_darkestsecret(0);
	this->book[n_reg] = add;
	this->n_reg++;
	return ;
}

void
Phonebook::func_search(int option){
	if (this->n_reg){
		if (!option)
			this->func_print(3);
		std::string 	s;
		std::getline(std::cin, s);
		std::cin.clear();

		int num = atoi(s.c_str());
		if (num >= 1 && num <= this->n_reg){
			std::cout << '\n';
			this->book[num - 1].func_firstname(1);
			this->book[num - 1].func_lastname(1);
			this->book[num - 1].func_nickname(1);
			this->book[num - 1].func_login(1);
			this->book[num - 1].func_postaladdress(1);
			this->book[num - 1].func_emailaddress(1);
			this->book[num - 1].func_phonenumber(1);
			this->book[num - 1].func_birthdaydate(1);
			this->book[num - 1].func_favoritemeal(1);
			this->book[num - 1].func_underwearcolor(1);
			this->book[num - 1].func_darkestsecret(1);
		}
		else {
			std::cout << "❌ Erreur, entrez un numéro d'index valide : ";
			this->func_search(1);
		}
	}
	else
		this->func_print(2);
	return ;
}

void
Phonebook::func_exit(void) const{
	std::cout << "FERMETURE DU PROGRAMME ..." << std::endl;
	return ;
}

void
Phonebook::func_boucle(void){
	int 			stop = 0;
	std::string 	buf;

	this->func_print(0);
	while (!stop){
		std::cout << "$";
		std::getline(std::cin, buf);
		buf.erase(std::remove_if(buf.begin(), buf.end(), ::isspace), buf.end());
		if (!strcmp(buf.c_str(), "ADD"))
			this->func_add();
		else if (!strcmp(buf.c_str(), "SEARCH"))
			this->func_search(0);
		else if (!strcmp(buf.c_str(), "EXIT")){
			this->func_exit();
			stop++;
		}
		else
			std::cout << "❌ Commande : non valide" << std::endl;
		std::cin.clear();
	}
	return ;
}
